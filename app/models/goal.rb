class Goal < ApplicationRecord
  
	# == Constants ============================================================
	STATUSES = { 
    "NOT_STARTED" => 1, 
    "IN_PROGRESS" => 2, 
    "COMPLETED" => 3
  }

  # == Relationships ========================================================
  belongs_to :user
  has_many :key_results

  # == Validations ==========================================================
  validates :title, presence: true
  validates :end_date, date: { after_or_equal_to: :start_date }

  # == Methods ==============================================================
  # Returns the progress of a goal, that is the ratio of total number of key 
  # results with completed status divided by the total number of key results.
  def progress_ratio
  	ratio = 0
  	key_results_size = key_results.size
  	if key_results.size > 0
  		ratio = key_results.where(status_id: STATUSES["COMPLETED"]).size.to_f / key_results_size.to_f
  	end
  	return ratio * 100
  end

end
