class User < ApplicationRecord
	
	# == Relationships ========================================================
	has_many :goals
	
	# == Validations ==========================================================
  validates :name, presence: true
  validates :email, email: true
  
end
