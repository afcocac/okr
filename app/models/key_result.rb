class KeyResult < ApplicationRecord

	# == Relationships ========================================================
  belongs_to :status
  belongs_to :goal

  # == Validations ==========================================================
  validates :title, presence: true

end