module Api::V1
  class KeyResultsController < ApplicationController

    # GET /key_results/1
    def show
      @key_result = KeyResult.find(params[:id])
      render json: @key_result
    end

    # POST /key_results
    def create
      @key_result = KeyResult.new(key_result_params)

      if @key_result.save
        render json: @key_result, status: :created, location: api_v1_key_result_url(@key_result)
      else
        render json: @key_result.errors, status: :unprocessable_entity
      end
    end

    private
      def key_result_params
        params.require(:key_result).permit(:title, :status_id, :goal_id)
      end
  end
end