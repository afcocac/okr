module Api::V1
  class GoalsController < ApplicationController

    # GET /goals/1
    def show
      @goal = Goal.find(params[:id])
      render json: @goal
    end

    # GET /goals/user/1
    def user_goals
      @goals = Goal.where(user_id: params[:user_id])
    end

    # POST /goals
    def create
      @goal = Goal.new(goal_params)

      if @goal.save
        render json: @goal, status: :created, location: api_v1_goal_url(@goal)
      else
        render json: @goal.errors, status: :unprocessable_entity
      end
    end

    private
      def goal_params
        params.require(:goal).permit(:title, :start_date, :end_date, :user_id)
      end
  end
end