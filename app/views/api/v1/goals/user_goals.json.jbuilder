json.array! @goals do |goal|
  json.title goal.title
  json.start_date goal.start_date
  json.end_date goal.end_date
  json.progress goal.progress_ratio
end