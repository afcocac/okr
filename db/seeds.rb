# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Status.create(name: 'Not started')
Status.create(name: 'In progress')
Status.create(name: 'Completed')
User.create(name: 'Andres', email:'afcocac@gmail.com')
