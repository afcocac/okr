require "test_helper"

class KeyResultsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @key_result = key_results(:one)
  end

  test "should create key_result" do
    assert_difference('KeyResult.count') do
      post api_v1_key_results_url, params: { key_result: { goal_id: @key_result.goal_id, status_id: @key_result.status_id, title: @key_result.title } }, as: :json
    end

    assert_response 201
  end

  test "should show key_result" do
    get api_v1_key_result_url(@key_result), as: :json
    assert_response :success
  end

end
