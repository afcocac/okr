require "test_helper"
require 'securerandom'

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
  end

  test "should create user" do
    assert_difference('User.count') do
      post api_v1_users_url, params: { user: { email: "#{SecureRandom.hex}#{@user.email}", name: @user.name } }, as: :json
    end

    assert_response 201
  end

  test "should show user" do
    get api_v1_user_url(@user), as: :json
    assert_response :success
  end

end
