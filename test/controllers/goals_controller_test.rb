require "test_helper"

class GoalsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @goal = goals(:one)
  end

  test "should create goal" do
    assert_difference('Goal.count') do
      post api_v1_goals_url, params: { goal: { end_date: @goal.end_date, start_date: @goal.start_date, title: @goal.title, user_id: @goal.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show goal" do
    get api_v1_goal_url(@goal), as: :json
    assert_response :success
  end

end
