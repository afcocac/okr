Rails.application.routes.draw do
	namespace :api, defaults: { format: :json } do
		namespace :v1 do
		  resources :users
		  resources :key_results
		  resources :goals do
		  	collection do
		  		get 'user/:user_id', to: 'goals#user_goals' 
		  	end
		  end
		end
	end
end
